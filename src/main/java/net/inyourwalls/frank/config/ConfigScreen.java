// ConfigScreen: Frank's configuration screen.
package net.inyourwalls.frank.config;

import net.inyourwalls.frank.Frank;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.tooltip.Tooltip;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import java.io.IOException;

import static net.minecraft.client.gui.widget.ButtonWidget.DEFAULT_WIDTH;
import static net.minecraft.client.gui.widget.ButtonWidget.DEFAULT_WIDTH_SMALL;
import static net.minecraft.client.gui.widget.ButtonWidget.DEFAULT_HEIGHT;

public class ConfigScreen extends Screen {
    private static final Identifier FURNACE_UI_TEXTURE = Identifier.ofVanilla("textures/gui/container/furnace.png");
    private static final int FURNACE_TEXTURE_WIDTH = 176;
    private static final int FURNACE_TEXTURE_HEIGHT = 166;
    private static final Text DISPLAY_MODE_LABEL = Text.translatable("ui.frank.config.display_mode");
    private static final Text TEXT_COLOUR_LABEL = Text.translatable("ui.frank.config.text_colour");
    private final FrankConfig modifiedConfig = new FrankConfig();
    private final Screen previousScreen;

    public ConfigScreen(Screen previous, FrankConfig currentConfig) {
        super(Text.translatable("ui.frank.config"));
        this.previousScreen = previous;
        modifiedConfig.setDisplayMode(currentConfig.getDisplayMode());
        modifiedConfig.setTextColour(currentConfig.getTextColour()); 
    }

    @Override
    public void init() {
        ButtonWidget backButton = ButtonWidget.builder(Text.translatable("gui.back"), thisButton -> this.close())
            .size(DEFAULT_WIDTH, DEFAULT_HEIGHT)
            .position(this.width / 2 - DEFAULT_WIDTH - 4, this.height - 28)
            .build();

        ButtonWidget saveButton = ButtonWidget.builder(Text.translatable("selectWorld.edit.save"), thisButton -> {
            try {
                Frank.saveConfig(modifiedConfig);
                Frank.LOGGER.debug("Configuration file was successfully saved.");
            } catch (IOException ex) {
                Frank.LOGGER.error("Failed to save configuration file", ex);
            }
        })
        .size(DEFAULT_WIDTH, DEFAULT_HEIGHT)
        .position(this.width / 2 + 4, this.height - 28)
        .build();

        ButtonWidget modeToggleButton = ButtonWidget.builder(
            Text.translatable(modifiedConfig.getDisplayMode().translationKey()),
            thisButton -> {
                int ordinal = modifiedConfig.getDisplayMode().ordinal();
                FrankConfig.InfoDisplayMode[] infoDisplayModes = FrankConfig.InfoDisplayMode.values();
                if (++ordinal >= infoDisplayModes.length) {
                    ordinal = 0;
                }

                modifiedConfig.setDisplayMode(infoDisplayModes[ordinal]);
                thisButton.setMessage(Text.translatable(modifiedConfig.getDisplayMode().translationKey()));
            }
        )
        .size(DEFAULT_WIDTH_SMALL, DEFAULT_HEIGHT)
        .position(this.width / 2 + 4, 25)
        .build();

        TextFieldWidget colourField = new TextFieldWidget(
            this.textRenderer,
            this.width / 2 + 4,
            55,
            DEFAULT_WIDTH_SMALL,
            DEFAULT_HEIGHT,
            TEXT_COLOUR_LABEL
        );
        colourField.setChangedListener(input -> {
            try {
                int newColour = Integer.parseInt(input, 16);
                modifiedConfig.setTextColour(newColour); 
            } catch (NumberFormatException ex) { /* how */ }
        });
        colourField.setTextPredicate(text ->
            text.length() <= 6 && text.chars().allMatch(c ->
                 (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F') || (c >= '0' && c <= '9')
            )
        );
        colourField.setText(String.format("%x", modifiedConfig.getTextColour()));
        colourField.setTooltip(Tooltip.of(Text.translatable("ui.frank.config.text_colour.tooltip")));

        this.addDrawableChild(backButton);
        this.addDrawableChild(saveButton);
        this.addDrawableChild(modeToggleButton);
        this.addDrawableChild(colourField);
    }

    @Override
    public void close() {
        this.client.setScreen(this.previousScreen);
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        super.render(context, mouseX, mouseY, delta);
        context.drawCenteredTextWithShadow(this.textRenderer, Text.translatable("ui.frank.config"), this.width / 2, 5, 0xffffff);

        int labelX = this.width / 2 - Math.max(this.textRenderer.getWidth(DISPLAY_MODE_LABEL), this.textRenderer.getWidth(TEXT_COLOUR_LABEL)) - 4;
        context.drawTextWithShadow(
            this.textRenderer,
            DISPLAY_MODE_LABEL,
            labelX,
            30,
            0xffffff
        );
        context.drawTextWithShadow(
            this.textRenderer,
            TEXT_COLOUR_LABEL,
            labelX,
            60,
            0xffffff
        );

        renderPreview(context, this.width / 2 - FURNACE_TEXTURE_WIDTH / 2, this.height / 2 - FURNACE_TEXTURE_HEIGHT / 2);
    }

    private void renderPreview(DrawContext context, int x, int y) {
        context.drawTexture(
            RenderLayer::getGuiTextured,
            FURNACE_UI_TEXTURE,
            x,
            y,
            0,
            0,
            FURNACE_TEXTURE_WIDTH,
            FURNACE_TEXTURE_HEIGHT,
            256,
            256
        );
        drawCenteredText(
            context,
            this.textRenderer,
            Text.translatable("ui.frank.config.preview"),
            x + (FURNACE_TEXTURE_WIDTH / 2),
            y + 6,
            0x404040
        );

        int infoX = x + FURNACE_TEXTURE_WIDTH + 2;
        int infoY = y + 3;
        switch (modifiedConfig.getDisplayMode()) {
            case TIME_LEFT_FIRST -> {
                context.drawTextWithShadow(
                    this.textRenderer,
                    Text.translatable("ui.frank.time_left", 0, 0),
                    infoX,
                    infoY,
                    modifiedConfig.getTextColour()
                );
                context.drawTextWithShadow(
                    this.textRenderer,
                    Text.translatable("ui.frank.smeltable", 0),
                    infoX,
                    infoY + 10,
                    modifiedConfig.getTextColour()
                );
                break;
            }

            case TIME_LEFT_ONLY -> {
                context.drawTextWithShadow(
                    this.textRenderer,
                    Text.translatable("ui.frank.time_left", 0, 0),
                    infoX,
                    infoY,
                    modifiedConfig.getTextColour()
                );
                break;
            }

            case ITEMS_SMELTABLE_FIRST -> {
                context.drawTextWithShadow(
                    this.textRenderer,
                    Text.translatable("ui.frank.smeltable", 0),
                    infoX,
                    infoY,
                    modifiedConfig.getTextColour()
                );
                context.drawTextWithShadow(
                    this.textRenderer,
                    Text.translatable("ui.frank.time_left", 0, 0),
                    infoX,
                    infoY + 10,
                    modifiedConfig.getTextColour()
                );
                break;
            }

            case ITEMS_SMELTABLE_ONLY -> {
                context.drawTextWithShadow(
                    this.textRenderer,
                    Text.translatable("ui.frank.smeltable", 0),
                    infoX,
                    infoY,
                    modifiedConfig.getTextColour()
                );
                break;
            }

            case DISABLED -> {}
        }
    }

    private void drawCenteredText(DrawContext context, TextRenderer renderer, Text text, int centerX, int y, int colour) {
        context.drawText(renderer, text, centerX - renderer.getWidth(text) / 2, y, colour, false);
    }
}
