// FrankConfig: Config class for Frank.
package net.inyourwalls.frank.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

public class FrankConfig {
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    private InfoDisplayMode infoDisplayMode = InfoDisplayMode.TIME_LEFT_FIRST;
    private int textColour = 0xffffff;

    /**
    * Gets the "mode" Frank should display its information in.
    */
    public InfoDisplayMode getDisplayMode() {
        return this.infoDisplayMode;
    }

    /**
    * Sets the "mode" Frank should display its information in.
    */
    public void setDisplayMode(InfoDisplayMode infoDisplayMode) {
        this.infoDisplayMode = infoDisplayMode;
    }

    /**
    * Gets the current text colour.
    */
    public int getTextColour() {
        return this.textColour;
    }

    /**
    * Sets a new text colour.
    */
    public void setTextColour(int textColour) {
        this.textColour = textColour;
    }

    /**
    * Attempts to load the configuration from the given path. If the file is
    * empty, all values will be set to default.
    * @param configPath The path to try loading the configuration from.
    * @throws IOException If any I/O errors occur while loading the config.
    */
    public static FrankConfig load(Path configPath) throws IOException {
        FileReader reader = new FileReader(configPath.toFile());
        FrankConfig loadedConfig = GSON.fromJson(reader, FrankConfig.class);
        return loadedConfig == null ? new FrankConfig() : loadedConfig;
    }

    /**
    * Attempts to save the given configuration to the given path.
    * @param config The configuration to save.
    * @param configPath The path to try saving the configuration to.
    * @throws IOException If any errors occur while saving the config.
    */
    public static void save(FrankConfig config, Path configPath) throws IOException {
        FileWriter writer = new FileWriter(configPath.toFile());
        GSON.toJson(config, writer);
        writer.close();
    }

    /**
    * An enum representing the five ways Frank can display its information.
    */
    public static enum InfoDisplayMode {
        TIME_LEFT_FIRST,
        TIME_LEFT_ONLY,
        ITEMS_SMELTABLE_FIRST,
        ITEMS_SMELTABLE_ONLY,
        DISABLED;

        public String translationKey() {
            return "ui.frank.config." + this.toString().toLowerCase();
        }
    }
}
