package net.inyourwalls.frank;

public interface ScreenHandlerPropertyAccessor {
    default int getRemainingFuelTicks() {
        return 0;
    }

    default int getTotalFuelTicks() {
        return 0;
    }

    default int getCurrentCookingTicks() {
        return 0;
    }

    default int getTotalCookingTicks() {
        return 0;
    }
}
