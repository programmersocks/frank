package net.inyourwalls.frank.mixin;

import net.inyourwalls.frank.ScreenHandlerPropertyAccessor;
import net.minecraft.screen.AbstractFurnaceScreenHandler;
import net.minecraft.screen.PropertyDelegate;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(AbstractFurnaceScreenHandler.class)
public abstract class AbstractFurnaceScreenHandlerMixin implements ScreenHandlerPropertyAccessor {
    @Shadow
    @Final
    private PropertyDelegate propertyDelegate;

    @Override
    public int getRemainingFuelTicks() {
        return this.propertyDelegate.get(0);
    }

    @Override
    public int getTotalFuelTicks() {
        return this.propertyDelegate.get(1);
    }

    @Override
    public int getCurrentCookingTicks() {
        return this.propertyDelegate.get(2);
    }

    @Override
    public int getTotalCookingTicks() {
        return this.propertyDelegate.get(3);
    }
}
