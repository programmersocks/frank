// ModMenuIntegration: Entrypoint for integrating with Mod Menu.
package net.inyourwalls.frank;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import net.inyourwalls.frank.config.ConfigScreen;

public class ModMenuIntegration implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return parent -> {
            return new ConfigScreen(parent, Frank.getConfig());
        };
    }
}
