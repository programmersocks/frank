// Frank: Minecraft mod that displays additional information in the furnace UI.
package net.inyourwalls.frank;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.fabric.api.client.screen.v1.ScreenEvents;
import net.inyourwalls.frank.config.FrankConfig;
import net.minecraft.client.gui.screen.ingame.AbstractFurnaceScreen;
import net.minecraft.screen.AbstractFurnaceScreenHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;

public class Frank implements ClientModInitializer {
    public static final Logger LOGGER = LoggerFactory.getLogger(Frank.class);
    private static final Path CONFIG_PATH = FabricLoader.getInstance().getConfigDir().resolve("frank.json");
    private static FrankConfig config;

    @Override
    public void onInitializeClient() {
        try {
            config = FrankConfig.load(CONFIG_PATH);
        } catch (IOException ex) {
            LOGGER.warn("Failed to load configuration file, falling back on default values.");
            config = new FrankConfig();
        }

        ScreenEvents.AFTER_INIT.register((client, screen, scaledWidth, scaledHeight) -> {
            if (screen instanceof AbstractFurnaceScreen furnace) {
                AbstractFurnaceScreenHandler screenHandler = (AbstractFurnaceScreenHandler) furnace.getScreenHandler();
                ScreenEventHandler handler = new ScreenEventHandler(furnace, screenHandler, client, config);
                ScreenEvents.afterRender(screen).register(handler);
                ScreenEvents.afterTick(screen).register(handler);
            }
        });
        LOGGER.info("Screen events registered.");
    }

    /**
    * Gets the current active configuration.
    */
    public static FrankConfig getConfig() {
        return config;
    }

    /**
    * Attempts to save the provided configuration.
    */
    public static void saveConfig(FrankConfig configToSave) throws IOException {
        config = configToSave;
        FrankConfig.save(config, CONFIG_PATH);
    }
}

