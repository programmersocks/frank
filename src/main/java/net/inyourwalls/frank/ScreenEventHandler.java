package net.inyourwalls.frank;

import net.fabricmc.fabric.api.client.screen.v1.ScreenEvents;
import net.inyourwalls.frank.config.FrankConfig;
import net.inyourwalls.frank.mixin.RecipeBookScreenAccessor;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.AbstractFurnaceScreen;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AbstractFurnaceScreenHandler;
import net.minecraft.text.Text;

public class ScreenEventHandler implements ScreenEvents.AfterRender, ScreenEvents.AfterTick {
    private static final int CRAFTING_SLOT_INDEX = 0;
    private static final int FUEL_SLOT_INDEX = 1;
    private final AbstractFurnaceScreen<?> screen;
    private final AbstractFurnaceScreenHandler screenHandler;
    private final MinecraftClient client;
    private final FrankConfig config;
    private int ticksLeft = 0;
    private int itemsSmeltable = 0;

    public ScreenEventHandler(
        AbstractFurnaceScreen<?> screen,
        AbstractFurnaceScreenHandler screenHandler,
        MinecraftClient client,
        FrankConfig config
    ) {
        this.screen = screen;
        this.screenHandler = screenHandler;
        this.client = client;
        this.config = config;
    }

    @Override
    public void afterRender(Screen screen, DrawContext drawContext, int mouseX, int mouseY, float tickDelta) {
        int secondsLeft = this.ticksLeft / 20;
        int x = this.screen.width / 2 + (((RecipeBookScreenAccessor) this.screen).getRecipeBook().isOpen() ? 167 : 90);
        int y = this.screen.height / 2 - 80; 
        switch (this.config.getDisplayMode()) {
            case TIME_LEFT_FIRST -> {
                renderTimeLeft(drawContext, secondsLeft, x, y);
                renderItemsSmeltable(drawContext, x, y + 10);
            }

            case TIME_LEFT_ONLY -> {
                renderTimeLeft(drawContext, secondsLeft, x, y);
            }

            case ITEMS_SMELTABLE_FIRST -> {
                renderItemsSmeltable(drawContext, x, y);
                renderTimeLeft(drawContext, secondsLeft, x, y + 10);
            }

            case ITEMS_SMELTABLE_ONLY -> {
                renderItemsSmeltable(drawContext, x, y);
            }

            case DISABLED -> {}
        }

        if (this.client.getDebugHud().shouldShowDebugHud()) {
            renderDebugValues(drawContext, x, y + 166 - 45); 
        }
    }

    private void renderTimeLeft(DrawContext drawContext, int secondsLeft, int x, int y) {
        int minutesLeft = secondsLeft / 60;
        secondsLeft %= 60;
        drawContext.drawTextWithShadow(
            this.client.textRenderer,
            Text.translatable("ui.frank.time_left", minutesLeft, secondsLeft),
            x,
            y,
            this.config.getTextColour()
        );
    }

    private void renderItemsSmeltable(DrawContext drawContext, int x, int y) {
        drawContext.drawTextWithShadow(
            this.client.textRenderer,
            Text.translatable("ui.frank.smeltable", itemsSmeltable),
            x,
            y,
            this.config.getTextColour()
        );
    }

    private void renderDebugValues(DrawContext drawContext, int x, int y) {
        drawContext.drawTextWithShadow(
            this.client.textRenderer,
            Text.literal("remainingFuelTicks: " + screenHandler.getRemainingFuelTicks()),
            x,
            y,
            0xffffff
        );
        drawContext.drawTextWithShadow(
            this.client.textRenderer,
            Text.literal("totalFuelTicks: " + screenHandler.getTotalFuelTicks()),
            x,
            y + 10,
            0xffffff
        );
        drawContext.drawTextWithShadow(
            this.client.textRenderer,
            Text.literal("currentCookingTicks: " + screenHandler.getCurrentCookingTicks()),
            x,
            y + 20,
            0xffffff
        );
        drawContext.drawTextWithShadow(
            this.client.textRenderer,
            Text.literal("totalCookingTicks: " + screenHandler.getTotalCookingTicks()),
            x,
            y + 30,
            0xffffff
        );
    }

    @Override
    public void afterTick(Screen screen) {
        ItemStack fuel = this.screenHandler.getSlot(FUEL_SLOT_INDEX).getStack();
        int availableFuelTicks = this.client.world.getFuelRegistry().getFuelTicks(fuel);
        availableFuelTicks *= fuel.getCount();
        int itemsSmeltable = availableFuelTicks / 200;
        this.itemsSmeltable = itemsSmeltable + this.screenHandler.getRemainingFuelTicks() / (this.screenHandler.isBurning() ? this.screenHandler.getTotalCookingTicks() : 1);
        int totalTicks =
            this.screenHandler.getSlot(CRAFTING_SLOT_INDEX).getStack().getCount() * this.screenHandler.getTotalCookingTicks();
        this.ticksLeft = totalTicks - this.screenHandler.getCurrentCookingTicks();
    }
}
