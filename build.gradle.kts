plugins {
    java
    id("fabric-loom") version "1.9-SNAPSHOT"
}

val minecraftVersion = "1.21.4"
version = "1.0.3+$minecraftVersion"
group = "net.inyourwalls"

repositories {
    // Repositories to fetch additional dependencies from.
    maven("https://maven.terraformersmc.com/")
}

dependencies {
    // Fabric/Minecraft dependencies.
    val yarnBuild = "8"
    minecraft("com.mojang:minecraft:$minecraftVersion")
    mappings("net.fabricmc:yarn:$minecraftVersion+build.$yarnBuild:v2")
    modImplementation("net.fabricmc:fabric-loader:0.14.23")
    modImplementation("net.fabricmc.fabric-api:fabric-api:0.114.2+$minecraftVersion")

    modImplementation("com.terraformersmc:modmenu:9.0.0")
}

tasks {
    processResources {
        inputs.property("version", project.version)
        filesMatching("fabric.mod.json") {
            expand(mapOf(
                "version" to project.version
            ))
        }
    }
}
