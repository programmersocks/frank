# frank

Simple Minecraft mod that displays a bit of extra information in furnaces.

## Features

* Displays time remaining and total items smeltable in the furnace UI.
* Configurable line ordering & text colour.

## Installation

The mod can be downloaded from [Modrinth](https://modrinth.com/mod/frank).
Alternatively, you can clone the mod
[source code](https://codeberg.org/inyourwalls/frank) and compile it yourself.

Frank requires [Fabric API](https://modrinth.com/mod/fabric-api). The config
screen requires [Mod Menu](https://modrinth.com/mod/modmenu) to be accessed.

## Support

If you require support, you can join the Matrix room at
[`#mod-support:inyourwalls.net`](https://matrix.to/#/#mod-support:inyourwalls.net).
