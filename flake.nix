{
  description = "Development environment for frank.";

  inputs.nixpkgs = {
    url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }: {
    devShells.x86_64-linux.default = let pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in pkgs.mkShell {
      nativeBuildInputs = with pkgs.buildPackages; [
        gradle
        jdk21
        jdt-language-server
      ];
    };
  };
}
